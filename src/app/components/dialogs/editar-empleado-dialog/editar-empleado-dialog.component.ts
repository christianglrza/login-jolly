import { Component, OnInit,Inject } from '@angular/core';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder,FormGroup } from '@angular/forms';
import { Empleado } from 'src/app/interfaces/Empleado';

@Component({
  selector: 'app-editar-empleado-dialog',
  templateUrl: './editar-empleado-dialog.component.html',
  styleUrls: ['./editar-empleado-dialog.component.scss']
})
export class EditarEmpleadoDialogComponent implements OnInit {
  empleadoEditForm:FormGroup
  file:File|string

  constructor(private dialogRef: MatDialogRef<EditarEmpleadoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data:Empleado,
    private formBuilder:FormBuilder) { }

  ngOnInit() {    
    this.empleadoEditForm = this.formBuilder.group({
      brm:[this.data.brm],
      nombre:[this.data.nombre],
      puesto:[this.data.puesto],
      foto:[this.data.foto]
    })
    console.log(this.data);
    
  }

  editEmpleado():void {
    const fotoTmp = this.data.foto
    this.data.brm = this.empleadoEditForm.get('brm').value
    this.data.nombre = this.empleadoEditForm.get('nombre').value
    this.data.puesto = this.empleadoEditForm.get('puesto').value
    this.data.foto = this.empleadoEditForm.get('foto').value
    if(this.data.foto = fotoTmp){
      const url = `data:image/jpg;base64,${this.data.foto}`
      fetch(url)
      .then(res => res.blob())
      .then(blob => {
        this.file = new File([blob], "File name",{ type: "image/jpg" })
        this.data.foto = this.file
      })
    }
    this.dialogRef.close(this.data)
  }

  onImageUpload(event){
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.empleadoEditForm.get('foto').setValue(file)
    }    
  }

}
