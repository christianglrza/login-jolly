import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarEmpleadoDialogComponent } from './editar-empleado-dialog.component';

describe('EditarEmpleadoDialogComponent', () => {
  let component: EditarEmpleadoDialogComponent;
  let fixture: ComponentFixture<EditarEmpleadoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarEmpleadoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarEmpleadoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
