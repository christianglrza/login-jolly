import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  leftContent: any = [{ description: 'Opciones', isTitle: true },];


  usuario:any

  constructor() { }

  ngOnInit() {
    this.usuario = JSON.parse(localStorage.getItem('usuario'))
    this.usuario.rol[0].name_rol === 'empleado' 
    ? this.leftContent.push({description: 'Empleado', isTitle: false,route: 'inicio/empleado'}) 
    : this.leftContent.push({description: 'Jugador',isTitle: false,route: 'inicio/jugador'}) 
  }

}
