import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginService } from '../../services/login.service';
import { EmpleadoService } from 'src/app/services/empleado.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  userForm:FormGroup

  constructor(private formBuilder:FormBuilder, 
    private loginService:LoginService,
    private empleadoService:EmpleadoService,
    public router:Router) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      username: [''],
      password: ['']
    });
  }

  postUsuario():void{
    this.loginService.login(this.userForm.value).subscribe(data=>{
      this.loginService.guardarUsuario(data)
      this.router.navigate(['inicio']);
    })
  }
}