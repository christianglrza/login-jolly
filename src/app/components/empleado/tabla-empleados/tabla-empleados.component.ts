import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Empleado } from 'src/app/interfaces/Empleado';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { MatDialog, MatDialogConfig } from "@angular/material";
import { EditarEmpleadoDialogComponent } from '../../dialogs/editar-empleado-dialog/editar-empleado-dialog.component';

@Component({
  selector: 'app-tabla-empleados',
  templateUrl: './tabla-empleados.component.html',
  styleUrls: ['./tabla-empleados.component.scss']
})
export class TablaEmpleadosComponent implements OnInit {
  @Output() eliminarEmpleado: EventEmitter<Empleado> = new EventEmitter
  @Output() editarEmpleado: EventEmitter<Empleado> = new EventEmitter

  columns:string[] = ['Brm','Nombre','Foto del Empleado', 'Puesto', 'Acciones']

  @Input() empleados:any[]=[{
    brm:'',
    nombre:'',
    puesto:'',
    foto: ''
  }]

  constructor(private empleadoService:EmpleadoService,private dialog: MatDialog) { }

  ngOnInit() {
  }

  borrar(empleado:Empleado):void {
    this.eliminarEmpleado.emit(empleado)
  }  

  editar(empleado:Empleado):void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    dialogConfig.data=empleado
    this.dialog.open(EditarEmpleadoDialogComponent,dialogConfig).afterClosed().subscribe(data=>{
      this.editarEmpleado.emit(data)
    });
  }
}