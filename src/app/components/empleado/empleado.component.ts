import { Component, OnInit } from '@angular/core';
import { AlertService } from '@next/nx-controls-common/services/alert.service';
import { Empleado } from 'src/app/interfaces/Empleado';

import { EmpleadoService } from 'src/app/services/empleado.service';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.scss']
})
export class EmpleadoComponent implements OnInit {
  empleados:any[]=[];

  constructor(private empleadoService:EmpleadoService,private alertService: AlertService) { }

  ngOnInit() {
    this.empleadoService.getEmpleados().subscribe(data=>{
      this.empleados.push(data)
      console.log(this.empleados);
    })
    
  }

  agregarEmpleado(empleado:Empleado):void {
    if(empleado.foto){
      const formData = new FormData();
      formData.append('brm',empleado.brm)
      formData.append('nombre',empleado.nombre)
      formData.append('puesto',empleado.puesto)
      formData.append('foto', empleado.foto)
      this.empleadoService.postEmpleado(formData).subscribe(data=>{
        this.empleados[0].push(data)
        this.success('Empleado agregado con éxito :D')
      })
    }
    else{
      this.warn('Imagen requerida D:') 
    }
  }

  eliminarEmpleado(empleado:any):void{
    this.empleados[0]=this.empleados[0].filter(t=>t.id!== empleado.id)    
    this.empleadoService.deleteEmpleado(empleado).subscribe()
    this.warn('Empleado eliminado con éxito')
  }

  editarEmpleado(empleado):void {
    console.log(empleado);
    const formData = new FormData();
    formData.append('brm',empleado.brm)
    formData.append('nombre',empleado.nombre)
    formData.append('puesto',empleado.puesto)
    formData.append('foto', empleado.foto)
    this.empleadoService.putEmpleado(empleado.id,formData).subscribe(data => {
      const index = this.empleados[0].findIndex(x => x.id == empleado.id)
      this.empleados[0][index] = data
      this.success('Empleado editado correctamente')
    })
  }

  warn(message: string) {
    this.alertService.warn(message);
  }

  success(message: string) {
    this.alertService.success(message);
  }
}
