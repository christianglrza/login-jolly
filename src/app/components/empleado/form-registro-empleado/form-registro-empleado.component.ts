import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { FormBuilder,FormGroup } from '@angular/forms';
import { Empleado } from 'src/app/interfaces/Empleado';

@Component({
  selector: 'app-form-registro-empleado',
  templateUrl: './form-registro-empleado.component.html',
  styleUrls: ['./form-registro-empleado.component.scss']
})
export class FormRegistroEmpleadoComponent implements OnInit {
  @Output() agregarEmpleado: EventEmitter<Empleado> = new EventEmitter()

  empleadoForm:FormGroup
  brm:string
  nombre:string
  puesto:string
  foto:File
  

  constructor(private formBuilder:FormBuilder) { }

  ngOnInit() {
    this.empleadoForm = this.formBuilder.group({
      brm:[''],
      nombre:[''],
      puesto:[''],
      foto:['']
    })
  }

  postEmpleado():void {
    const empleado:Empleado = {
      brm: this.empleadoForm.get('brm').value,
      nombre: this.empleadoForm.get('nombre').value,
      puesto: this.empleadoForm.get('puesto').value,
      foto: this.empleadoForm.get('foto').value,
    }
    this.agregarEmpleado.emit(empleado)
  }

  onImageUpload(event){
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.empleadoForm.get('foto').setValue(file)
    }    
  }
}