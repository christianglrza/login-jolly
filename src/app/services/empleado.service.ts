import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Empleado } from '../interfaces/Empleado';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {
  url:string = "https://aplicacion-empresarial.herokuapp.com/empleados"

  constructor(private http:HttpClient) { }

  getEmpleados():Observable<Empleado[]>{
    return this.http.get<Empleado[]>(this.url)
  }

  postEmpleado(empleado:any):Observable<Empleado>{
    return this.http.post<Empleado>(this.url,empleado)
  }

  deleteEmpleado(empleado:any):Observable<Empleado>{
    const url = `${this.url}/${empleado.id}`
    return this.http.delete<Empleado>(url)
  }

  putEmpleado(id:number,empleado:any):Observable<any>{
    const url = `${this.url}/${id}`
    return this.http.put(url,empleado)
  }
}