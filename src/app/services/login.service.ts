import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Login } from '../interfaces/Login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private url:string = "https://login-back-d10.herokuapp.com/login"

  constructor(private http:HttpClient) { }

  login(usuario:Login):Observable<Login>{    
    return this.http.post<Login>(this.url,usuario)
  }

  guardarUsuario(usuario){
    localStorage.setItem('usuario',JSON.stringify(usuario))
  }
}