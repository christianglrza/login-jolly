import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { CommonsModule } from '@next/nx-controls-common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RootComponent } from './components/root/root.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { EmpleadoComponent } from './components/empleado/empleado.component';
import { JugadorComponent } from './components/jugador/jugador.component';
import { FormRegistroEmpleadoComponent } from './components/empleado/form-registro-empleado/form-registro-empleado.component';
import { TablaEmpleadosComponent } from './components/empleado/tabla-empleados/tabla-empleados.component';
import { EditarEmpleadoDialogComponent } from './components/dialogs/editar-empleado-dialog/editar-empleado-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    RootComponent,
    LoginFormComponent,
    InicioComponent,
    EmpleadoComponent,
    JugadorComponent,
    FormRegistroEmpleadoComponent,
    TablaEmpleadosComponent,
    EditarEmpleadoDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonsModule.forRoot(),
    MatDialogModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[EditarEmpleadoDialogComponent]
})
export class AppModule { }
