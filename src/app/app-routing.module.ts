import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpleadoComponent } from './components/empleado/empleado.component';
import { HomeComponent } from './components/home/home.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { JugadorComponent } from './components/jugador/jugador.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { RootComponent } from './components/root/root.component';

const routes: Routes = [
  { path: '',    component: LoginFormComponent  },
  { path: 'inicio', component:  InicioComponent,children: [
    { path: 'empleado', component: EmpleadoComponent },
    { path: 'jugador', component: JugadorComponent }
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }