const path = require('path');
const express = require('express');
const app = express();

// Serve static files
app.use(express.static(__dirname + '/dist/nx-getting-started-ui-master@64d311091bd'));

// Send all requests to index.html
app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname + '/dist/nx-getting-started-ui-master@64d311091bd/index.html'));
});

// default Heroku port
app.listen(process.env.PORT || 5000);